import gulp from 'gulp';
import sass from 'gulp-sass';
import babel from 'gulp-babel';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import rename from 'gulp-rename';
import cleanCSS from 'gulp-clean-css';
import livereload from 'gulp-livereload';
import nodemon from 'gulp-nodemon';
import gulpif from 'gulp-if';

import del from 'del';
import path from 'path';
import browserify from 'browserify';
import babelify from 'babelify';
import vueify from 'vueify';
import stream from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';

import config from './config.json';

const env = process.env.NODE_ENV;
const dev = process.env.NODE_ENV === 'development';

console.log('learncraft server is running in', env, 'mode.');

export function clientStyles() {
  return gulp.src(config.paths.client.styles.src)
    .pipe(sass())
    .pipe(cleanCSS())
    .pipe(rename({
      basename: 'client',
      suffix: '.min'
    }))
    .pipe(gulp.dest(config.paths.client.styles.dest))
    .pipe(gulpif(dev, livereload()));
}

export function clientScripts() {

  let bundler = browserify({
    entries: config.paths.client.scripts.src,
    debug: true
  });
  return bundler
    .on('error', function (err) {
      console.error(err);
    })
    .transform(babelify, { presets: ["env"] })
    .transform(vueify)
    .bundle()
    .pipe(stream('client.min.js'))
    .pipe(buffer())
    .pipe(gulp.dest(config.paths.client.scripts.dest))
    .pipe(gulpif(dev, livereload()));
}

export function clientViews() {
  return gulp.src(config.paths.client.views.src)
    .pipe(gulp.dest(config.paths.client.views.dest))
    .pipe(gulpif(dev, livereload()));
}

export function clientAssets() {
  return gulp.src(config.paths.client.assets.src)
    .pipe(gulp.dest(config.paths.client.assets.dest))
    .pipe(gulpif(dev, livereload()));
}

export function serverScripts() {
  return gulp.src(config.paths.server.src, { sourcemaps: true })
    .pipe(babel())
    .pipe(gulpif(!dev, uglify()))
    //.pipe(concat('server.min.js'))
    .pipe(gulp.dest(config.paths.server.dest))
    .pipe(gulpif(dev, livereload()));
}

export function libScripts() {
  return gulp.src(config.paths.libs.src, { sourcemaps: true })
    .pipe(babel())
    .pipe(gulpif(!dev, uglify()))
    .pipe(gulp.dest(config.paths.libs.dest))
    .pipe(gulpif(dev, livereload()));
}

export function modelScripts() {
  return gulp.src(config.paths.models.src, { sourcemaps: true })
    .pipe(babel())
    .pipe(gulpif(!dev, uglify()))
    .pipe(gulp.dest(config.paths.models.dest))
    .pipe(gulpif(dev, livereload()));
}

export function watch() {
  livereload.listen();
  gulp.watch(config.paths.client.styles.src, clientStyles);
  gulp.watch(config.paths.client.scripts.src, clientScripts);
  gulp.watch(config.paths.client.views.src, clientViews);
  gulp.watch(config.paths.client.components.src, clientScripts);
  gulp.watch(config.paths.client.assets.src, clientAssets);
  gulp.watch(config.paths.server.src, server);
  gulp.watch(config.paths.libs.src, libScripts);
  gulp.watch(config.paths.models.src, modelScripts);
}

export function start() {
  nodemon({
    script: 'build/server/server.js',
    ext: 'js pug vue scss',
    watch: ['src'],
    ignore: [
      'gulpfile.babel.js'
    ]
  })
};

export const clean = () => del(['build']);
export const client = gulp.parallel(clientStyles, clientScripts, clientViews, clientAssets);
export const server = gulp.parallel(serverScripts);
export const libs = gulp.parallel(libScripts);
export const models = gulp.parallel(modelScripts);
// TODO: disable watch and nodemon in production mode
export const run = gulp.parallel(watch, start);

export const build = gulp.series(clean, gulp.parallel(client, server, libs, models), run);

export default build;