# learncraft
##### A learning and teaching platform with gamification
---

## Introduction

## Installation

1. download and install Git from https://git-scm.com/
2. download and install Node from https://nodejs.org/
3. download and install Yarn from https://yarnpkg.com/
4. clone the github repository
   ```sh
      $ cd /your/projects/directory
      $ git clone https://github.com/peddn/learncraft.git
      $ cd learncraft
   ```
5. install all dependencies
   ```sh 
      $ yarn install
   ```
6. run the application in developement mode
   ```sh
      $ npm run gulp-dev
   ```
   or in production mode
   ```sh
      $ npm run gulp-prod
   ```
7. open a browser and go to
   ```
      http://localhost:3000
   ```