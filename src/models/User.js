import mongoose from 'mongoose';
import { Schema } from 'mongoose';
import bcryptjs from 'bcryptjs';

const user = new Schema({
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  email: {
    type: String,
    unique: true,
    required: true,
    lowercase: true,
    trim: true
  },
  password: {
    type: String,
    required: true
  }
});

user.pre('save', function (next) {
  let salt = bcryptjs.genSaltSync(10);
  let hashedPassword = bcryptjs.hashSync(this.password, salt);
  this.password = hashedPassword;
  next();
});



class UserClass {

  public() {
    delete this.password;
    return this;
  }

  authenticate(password) {
    if (bcryptjs.compareSync(password, this.password)) {
      return this;
    } else {
      return false;
    }
  }

  static findByUsername(username) {
    return this.findOne({ username: username });
  }
  static findByEmail(email) {
    return this.findOne({ email: email });
  }

}

user.loadClass(UserClass);

export default mongoose.model('User', user);