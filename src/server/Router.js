import express from 'express';
import passport from 'passport';
import jwt from 'jsonwebtoken';

// Models
import User from '../models/User.js';

class Router extends express.Router {

    constructor() {

        super();

        this.get('/', function (req, res) {
            res.render('index', { title: 'learncraft', appName: 'learncraft' });
        });

        this.post('/register', function (req, res) {

            let email = req.body.email.toLowerCase() || '';
            let username = req.body.username;
            let password = req.body.password;

            User.count({
                email
            })
            .then(function(count) {
                if (count === 0) {
                    // TODO do validation of email, password and username
                    let user = new User({
                        username: username,
                        email: email,
                        password: password
                    });
                    user.save(function (error, user) {
                        if (error) {
                            res.send({ error: error.message });
                        } else {
                            res.send({ success: true });
                        }
                    })
                } else {
                    res.send({ error: 'A user is already registerd with this email address.' });
                }
            })
        });

        this.post('/auth', (req, res) => {
            
            let email = req.body.email;
            let password = req.body.password;

            if(email && password) {
                User.findOne({ email: email }, (error, user) => {

                    if(error) {
                        res.json({ error: error.message });
                    }
                    if(!user) {
                        res.json({ error: 'email not found' });
                    } else {
                        if(user.authenticate(password)) {
                            //TODO: make this secure! dont send the ID!
                            let payload = { id: user.id };
                            let token = jwt.sign(payload, req.app.locals.SECRET);
                            res.json({ token: 'Bearer ' + token });
                        } else {
                            res.json({ error: 'passwords mismatch' });
                        }
                    }
    
                });
            } else {
                res.json({ error : 'no email and or password sent' });
            }


        });

    }

}

export default Router;