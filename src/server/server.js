import http from 'http';
import path from 'path';
import fs from 'fs';
import crypto from 'crypto';

import express from 'express';
import favicon from 'express-favicon';
import bodyParser from 'body-parser';
import jwt from 'jsonwebtoken';

import mongoose from 'mongoose';
import SocketIo from 'socket.io';
import WebSocket from 'ws';

import passport from 'passport';
import passportJwt from 'passport-jwt';
import { ExtractJwt } from 'passport-jwt';
import { Strategy } from 'passport-jwt';

import Router from './Router.js';
import Message from '../libs/Message.js';

// Models
import User from '../models/User.js';


// connect to mongodb
mongoose.connect('mongodb://localhost/learncraft');

// the database connection
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('mongodb connected');
});

let app = express();

// app locals
const env = process.env.NODE_ENV;
app.locals.ENV = env;

const dev = process.env.NODE_ENV === 'development';
app.locals.ENV_DEVELOPMENT = dev;

// the secret key
// TODO: make this secure
app.locals.SECRET = crypto.randomBytes(64).toString('hex');

// add middleware
//let MongoStore = connectMongo(session);

app.use(passport.initialize());
//app.use(passport.session());

app.use(favicon());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());
app.use(new Router());

// statics
app.use('/jquery', express.static(path.normalize(path.join(__dirname + '/../../node_modules/jquery/dist/'))));
app.use('/materialize-css', express.static(path.normalize(path.join(__dirname + '/../../node_modules/materialize-css/dist/'))));
//app.use('/vue', express.static(path.normalize(path.join(__dirname + '/../../node_modules/vue/dist/'))));
//app.use('/vue-router', express.static(path.normalize(path.join(__dirname + '/../../node_modules/vue-router/dist/'))));
app.use('/pixi.js', express.static(path.normalize(path.join(__dirname + '/../../node_modules/pixi.js/dist/'))));

app.use('/assets', express.static(path.normalize(path.join(__dirname + '/../client/assets/'))));
app.use('/scripts', express.static(path.normalize(path.join(__dirname + '/../client/scripts/'))));
app.use('/styles', express.static(path.normalize(path.join(__dirname + '/../client/styles'))));


let jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = app.locals.SECRET;

let strategy = new Strategy(jwtOptions, (payload, next) => {
    console.log('playload received, payload');
    Users.findOne({ id: payload.id }, (user, error) => {
        if (error) {
            return next(error, false)
        }
        if (!user) {
            return next('no user with this email', false);
        } else {
            return next(null, user);
        }
    });
});


passport.use(strategy);

// set view Engine
app.set('views', path.join(__dirname + '/../client/views/'));
app.set('view engine', 'pug');

// create the http server
let server = http.Server(app);



let verifyClient = (info, cb) => {
    let token = info.req.headers['sec-websocket-protocol'];    
    jwt.verify(token, app.locals.SECRET, function (err, decoded) {
        if(err) {
            console.log(err);
            cb(false);
        } else {
            // get user and attach it to requerst
            User.findById(decoded.id, (error, user) => {
                if (error) {
                    console.log(error);
                    cb(false);
                }
                if (!user) {
                    console.log('no user found with this id');
                    cb(false);
                } else {
                    
                    info.req.user = user;
                    cb(true);
                }
        
            });
        }
    });
}

const wss = new WebSocket.Server({ server, verifyClient });

wss.on('connection', (socket, req) => {
    //const location = url.parse(req.url, true);
    // You might use location.query.access_token to authenticate or share sessions
    // or req.headers.cookie (see http://stackoverflow.com/a/16395220/151312)
    console.log('A User connected...');
    let user = req.user;

    socket.on('message', (message) => {
        function dirTree(filename) {
            let stats = fs.lstatSync(filename),
                info = {
                    path: filename,
                    name: path.basename(filename)
                };
        
            if (stats.isDirectory()) {
                info.type = "folder";
                info.children = fs.readdirSync(filename).map(function(child) {
                    return dirTree(filename + '/' + child);
                });
            } else {
                // Assuming it's a file. In real life it could be a symlink or
                // something else!
                info.type = "file";
            }
        
            console.log(info);
        }
    });

    socket.on('error', (error) => {
        console.log(error);
    });

    socket.on('close', (code, reason) => {
        console.log('A user disconnected');
        console.log('code: ', code, 'reason: ', reason);
    });

});




// start the http server
server.listen(3000, function () {
    console.log('learncraft server is listening on port 3000');
});
