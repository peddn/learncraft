class Message {
    constructor(json) {
        this.data = json;
    }
    static createJson(type, data) {
        return {
            type: type,
            data: data
        }
    }
    toString() {
        return JSON.stringify(this.data);
    }
    static fromString(jsonString) {
        let json = JSON.parse(jsonString);
        return new Message(json);
    }
}
export default Message;