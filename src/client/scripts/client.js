import Vue from 'vue';
import VueRouter from 'vue-router';

import Message from '../../libs/Message.js';

import App from '../components/app.vue';


import Register from '../components/modules/register.vue';

import Sandbox from '../components/modules/sandbox.vue';
import FileBrowser from '../components/modules/filebrowser.vue';

Vue.use(VueRouter);
Vue.config.devtools = false;

const routes = [
    {
        path: '/register',
        component: Register,
        beforeEnter: (to, from, next) => {
            console.log("before Enter register");
            next();
        }
    },

    {
        path: '/sandbox',
        component: Sandbox,
        beforeEnter: (to, from, next) => {
            console.log("before Enter sandbox");
            next();
        }
    },
    {
        path: '/files',
        component: FileBrowser,
        beforeEnter: (to, from, next) => {
            console.log("before Enter Filebrowser");
            next();
        }
    }
];


const router = new VueRouter({
    routes: routes
});

new Vue({
    el: '#app',
    router: router,
    render: function (createElement) {
        return createElement(App)
    }
})
